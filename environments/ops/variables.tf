variable "project" {
  default = "gitlab-ops"
}

variable "region" {
  default = "us-east1"
}

variable "environment" {
  default = "ops"
}

variable "dns_zone_name" {
  default = "gitlab.net"
}

variable "bootstrap_script_version" {
  default = 8
}

variable "oauth2_client_id_log_proxy" {
}

variable "oauth2_client_secret_log_proxy" {
}

variable "oauth2_client_id_dashboards" {
}

variable "oauth2_client_secret_dashboards" {
}

variable "oauth2_client_id_gitlab_ops" {
}

variable "oauth2_client_secret_gitlab_ops" {
}

variable "oauth2_client_id_monitoring" {
}

variable "oauth2_client_secret_monitoring" {
}

variable "oauth2_client_id_prometheus_dogfood" {
}

variable "oauth2_client_secret_prometheus_dogfood" {
}

variable "machine_types" {
  type = map(string)

  default = {
    "aptly"                 = "n1-standard-1"
    "chef"                  = "n1-standard-4"
    "consul"                = "n1-standard-1"
    "log-proxy"             = "n1-standard-1"
    "proxy"                 = "n1-standard-2"
    "bastion"               = "n1-standard-1"
    "dashboards"            = "n1-standard-2"
    "dashboards-com"        = "n1-highmem-4"
    "monitor"               = "n1-standard-8"
    "monitoring"            = "n1-standard-2"
    "gitlab-ops"            = "n1-standard-16"
    "runner-build"          = "n1-standard-32"
    "runner-chatops"        = "n1-standard-8"
    "runner-release"        = "n1-standard-8"
    "runner-release-single" = "n1-standard-1"
    "runner-snapshots"      = "n1-standard-1"
    "blackbox"              = "n1-standard-1"
    "sentry"                = "n1-standard-16"
    "sd-exporter"           = "n1-standard-1"
    "thanos-compact"        = "n1-standard-2"
    "thanos-rule"           = "n1-standard-2"
    "thanos-store-global"   = "n1-highmem-2"
    "gitlab-gke"            = "n1-standard-4"
    "nessus"                = "n1-standard-4"
    "internal-grafana-db"   = "db-f1-micro"
    "public-grafana-db"     = "db-f1-micro"
  }
}

variable "monitoring_hosts" {
  type = map(list(string))

  default = {
    "names" = ["prometheus"]
    "ports" = [9090, 10902]
  }
}

variable "service_account_email" {
  type = string

  default = "terraform@gitlab-ops.iam.gserviceaccount.com"
}

# The ops network is allocated
# 10.250.0.0/16

variable "subnetworks" {
  type = map(string)

  default = {
    # us-east1
    "logging"                 = "10.250.1.0/24"
    "bastion"                 = "10.250.2.0/24"
    "dashboards"              = "10.250.3.0/24"
    "gitlab-ops"              = "10.250.4.0/24"
    "proxy"                   = "10.250.5.0/24"
    "monitor"                 = "10.250.6.0/24"
    "runner"                  = "10.250.7.0/24"
    "monitoring"              = "10.250.8.0/24"
    "sentry"                  = "10.250.9.0/24"
    "runner-chatops"          = "10.250.10.0/24"
    "dashboards-com"          = "10.250.11.0/24"
    "runner-release"          = "10.250.12.0/24"
    "gitlab-ops-geo"          = "10.250.13.0/24"
    "sd-exporter"             = "10.250.15.0/24"
    "runner-snapshots"        = "10.250.17.0/24"
    "thanos-store"            = "10.250.18.0/24"
    "thanos-compact"          = "10.250.19.0/24"
    "aptly"                   = "10.250.20.0/24"
    "consul"                  = "10.250.21.0/24"
    "nonprod-proxy"           = "10.250.22.0/24"
    "prod-proxy"              = "10.250.23.0/24"
    "thanos-rule"             = "10.250.25.0/24"
    "gitlab-gke"              = "10.250.26.0/23"
    "gitlab-gke-pod-cidr"     = "10.252.0.0/16"
    "gitlab-gke-service-cidr" = "10.250.28.0/23"

    # us-central1
    "chef" = "10.253.5.0/24"
  }
}

variable "public_ports" {
  type = map(list(string))

  default = {
    "log-proxy"   = []
    "proxy"       = []
    "bastion"     = [22]
    "dashboards"  = []
    "gitlab-ops"  = [443, 80, 5005, 2222]
    "runner"      = []
    "blackbox"    = []
    "sentry"      = [443, 80]
    "sd-exporter" = []
    "thanos"      = []
    "nessus"      = [8834]
    "aptly"       = [80, 443]
    "chef"        = [80, 443, 9683]
    "consul"      = []
  }
}

variable "node_count" {
  type = map(string)

  default = {
    "bastion"             = 1
    "blackbox"            = 1
    "chef"                = 1
    "consul"              = 1
    "dashboards"          = 1
    "gitlab-ops"          = 1
    "nessus"              = 1
    "prometheus"          = 2
    "runner"              = 1
    "sentry"              = 1
    "sd-exporter"         = 1
    "thanos-compact"      = 1
    "thanos-rule"         = 2
    "thanos-store"        = 2
    "thanos-store-global" = 2
  }
}

variable "master_cidr_subnets" {
  type = map(string)

  default = {
    "gitlab-gke" = "172.16.0.0/28"
  }
}

variable "chef_provision" {
  type        = map(string)
  description = "Configuration details for chef server"

  default = {
    bootstrap_bucket  = "gitlab-ops-chef-bootstrap"
    bootstrap_key     = "gitlab-ops-bootstrap-validation"
    bootstrap_keyring = "gitlab-ops-bootstrap"
    server_url        = "https://chef.gitlab.com/organizations/gitlab/"
    user_name         = "gitlab-ci"
    user_key_path     = ".chef.pem"
    version           = "14.13.11"
  }
}

variable "lb_fqdns_bastion" {
  type    = list(string)
  default = ["lb-bastion.ops.gitlab.com"]
}

variable "network_testbed" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-testbed/global/networks/testbed"
}

variable "network_ops" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-ops/global/networks/ops"
}

variable "network_gprd" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-production/global/networks/gprd"
}

variable "network_gstg" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-staging-1/global/networks/gstg"
}

variable "network_pre" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-pre/global/networks/pre"
}

variable "network_release" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-release/global/networks/release"
}

variable "network_org-ci" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-org-ci-0d24e2/global/networks/org-ci"
}

variable "tcp_lbs_bastion" {
  type = map(list(string))

  default = {
    "names"                  = ["ssh"]
    "forwarding_port_ranges" = ["22"]
    "health_check_ports"     = ["80"]
  }
}

variable "tcp_lbs_sentry" {
  type = map(list(string))

  default = {
    "names"                      = ["http", "https"]
    "forwarding_port_ranges"     = ["80", "443"]
    "health_check_ports"         = ["9000", "9000"]
    "health_check_request_paths" = ["/auth/login/gitlab/", "/auth/login/gitlab/"]
  }
}

variable "tcp_lbs_aptly" {
  type = map(list(string))

  default = {
    "names"                  = ["http", "https"]
    "forwarding_port_ranges" = ["80", "443"]
    "health_check_ports"     = ["80", "80"]
  }
}

variable "tcp_lbs_chef" {
  type = map(list(string))

  default = {
    "names"                      = ["http", "https", "bifrost"]
    "forwarding_port_ranges"     = ["80", "443", "9683"]
    "health_check_ports"         = ["80", "443", "9683"]
    "health_check_request_paths" = ["/", "/favicon.ico", "/"]
  }
}

variable "gcs_service_account_email" {
  type    = string
  default = "gitlab-object-storage@gitlab-ops.iam.gserviceaccount.com"
}

# Service account used to do automated backup testing
# in https://gitlab.com/gitlab-restore/postgres-gprd

variable "gcs_postgres_backup_service_account" {
  type    = string
  default = "postgres-wal-archive@gitlab-ops.iam.gserviceaccount.com"
}

variable "gcs_postgres_restore_service_account" {
  type    = string
  default = "postgres-automated-backup-test@gitlab-restore.iam.gserviceaccount.com"
}

variable "gcs_postgres_backup_kms_key_id" {
  type    = string
  default = "projects/gitlab-ops/locations/global/keyRings/gitlab-secrets/cryptoKeys/ops-postgres-wal-archive"
}

variable "postgres_backup_retention_days" {
  type    = string
  default = "5"
}

#######################
# pubsubbeat config
#######################

variable "pubsub_topics" {
  type = list(string)
  default = [
    "consul",
    "fluentd",
    "gitaly",
    "gke",
    "gke-audit",
    "gke-systemd",
    "monitoring",
    "pages",
    "postgres",
    "pubsubbeat",
    "rails",
    "redis",
    "registry",
    "runner",
    "shell",
    "sidekiq",
    "system",
    "workhorse",
  ]
}

variable "pubsub_filters" {
  type = map(string)
  default = {
    "gke-systemd" = "resource.type=k8s_node AND jsonPayload._SYSTEMD_UNIT=kubelet.service"
    "gke-audit"   = "(protoPayload.authenticationInfo:* OR protoPayload.method:* OR protoPayload.requestMetadata:*) AND resource.type!=\"gcs_bucket\""
    "gke"         = "log_name=projects/gitlab-ops/logs/events"
  }
}

### Object Storage Configuration

variable "artifact_age" {
  type    = string
  default = "30"
}

variable "upload_age" {
  type    = string
  default = "30"
}

variable "lfs_object_age" {
  type    = string
  default = "30"
}

variable "package_repo_age" {
  type    = string
  default = "30"
}

variable "storage_class" {
  type    = string
  default = "MULTI_REGIONAL"
}

variable "storage_log_age" {
  type    = string
  default = "7"
}

variable "gcs_storage_analytics_group_email" {
  type    = string
  default = "cloud-storage-analytics@google.com"
}

variable "cloudflare_zone_name" {}

variable "cloudflare_zone_id" {}

variable "cloudflare_api_key" {}

variable "cloudflare_account_id" {}

variable "cloudflare_email" {}

### Object Storage Configuration es-diagnostics

variable "gcs_gprd_es_diagnostics_versioning" {
  type    = string
  default = "false"
}

variable "gcs_gprd_es_diagnostics_age" {
  type    = string
  default = "90"
}

variable "gcs_gprd_es_diagnostics_storage_class" {
  type    = string
  default = "STANDARD"
}

variable "gcs_ops_es_diagnostics_versioning" {
  type    = string
  default = "false"
}

variable "gcs_ops_es_diagnostics_age" {
  type    = string
  default = "90"
}

variable "gcs_ops_es_diagnostics_storage_class" {
  type    = string
  default = "STANDARD"
}

# Ranges documented at https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/ci-runners/README.md#gitlab-org-ci-project
variable "org_ci_blocked_ranges" {
  default = ["10.2.0.0/16", "10.1.2.0/24", "10.1.0.0/24"]
}

variable "grafana_api_key" {
  type        = string
  description = "API Key used to talk to dashboards.gitlab.net"
}
