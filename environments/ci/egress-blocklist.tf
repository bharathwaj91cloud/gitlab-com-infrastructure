
module "egress-blocklist" {
  # Intentionally not version pinned. See
  # https://ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/egress-blocklist#noteworthy-details
  # tflint-ignore: terraform_module_pinned_source
  source      = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/egress-blocklist.git?ref=master"
  ip_set      = ""
  gcp_project = var.project
  gcp_network = "default"
  description = "https://gitlab.com/gitlab-com/gl-infra/production/-/issues/3885#note_528893616"
  name        = "block-miners-3885"
}
