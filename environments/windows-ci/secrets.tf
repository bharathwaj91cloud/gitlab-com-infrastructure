locals {

  secrets = [
    "bastion_windows-ci",
    "cookbook-gitlab-runner_windows-ci",
    "gitlab-omnibus-secrets_windows-ci",
  ]

  common_secret_readers = [
    "serviceAccount:terraform-ci@${var.project}.iam.gserviceaccount.com",
  ]

}
module "google-secrets" {
  source = "../../modules/google-secrets"
  secrets = {
    for i in local.secrets : i => { readers = local.common_secret_readers }
  }
}
