variable "gitlab_net_zone_id" {}

variable "environment" {
  default = "db-sharding-poc"
}
variable "project" {
  default = "gitlab-db-sharding-poc"
}

variable "region" {
  default = "us-east1"
}

variable "cloudflare_account_id" {}
variable "cloudflare_api_key" {}
variable "cloudflare_email" {}

variable "cloudflare_zone_id" {
  description = "Zone ID is injected in runtime from private/env_vars/db-sharding-poc.env (pulled from 1Password vault)"
}

variable "cloudflare_zone_name" {
  default = "gitlab.net"
}

variable "bootstrap_script_version" {
  default = 8
}

variable "chef_provision" {
  type        = map(string)
  description = "Configuration details for chef server"

  default = {
    bootstrap_bucket  = "gitlab-db-sharding-poc-chef-bootstrap"
    bootstrap_key     = "gitlab-db-sharding-poc-bootstrap-validation"
    bootstrap_keyring = "gitlab-db-sharding-poc-bootstrap"
    server_url        = "https://chef.gitlab.com/organizations/gitlab/"
    user_name         = "gitlab-ci"
    user_key_path     = ".chef.pem"
    version           = "14.13.11"
  }
}

variable "service_account_email" {
  type    = string
  default = "terraform@gitlab-db-sharding-poc.iam.gserviceaccount.com"
}

variable "gcs_service_account_email" {
  type    = string
  default = "gitlab-object-storage@gitlab-db-sharding-poc.iam.gserviceaccount.com"
}

variable "gcs_storage_analytics_group_email" {
  type    = string
  default = "cloud-storage-analytics@google.com"
}

variable "internal_subnets" {
  type    = list(string)
  default = ["10.0.0.0/16"]
}

variable "subnetworks" {
  type = map(string)

  default = {
    "bastion"    = "10.0.1.0/24"
    "all-in-one" = "10.0.2.0/24"
  }
}

variable "egress_ports" {
  type    = list(string)
  default = ["80", "443"]
}

variable "tcp_lbs_bastion" {
  type = map(list(string))

  default = {
    "names"                  = ["ssh"]
    "forwarding_port_ranges" = ["22"]
    "health_check_ports"     = ["80"]
  }
}

variable "lb_fqdns_bastion" {
  type    = list(string)
  default = ["lb-bastion.db-sharding-poc.gitlab.com"]
}

variable "machine_types" {
  type = map(string)

  default = {
    "bastion"    = "g1-small"
    "all-in-one" = "custom-8-16384"
  }
}

variable "node_count" {
  type = map(string)

  default = {
    "bastion"    = 1
    "all-in-one" = 1
  }
}

variable "public_ports" {
  type = map(list(string))

  default = {
    "bastion"    = [22]
    "all-in-one" = [22, 80, 443]
  }
}
