## State storage
terraform {
  backend "s3" {
    bucket = "gitlab-com-infrastructure"
    key    = "terraform/db-sharding-poc/terraform.tfstate"
    region = "us-east-1"
  }
}

## AWS
provider "aws" {
  region  = "us-east-1"
  version = "~> 2.27.0"
}

## Local
provider "local" {
  version = "~> 1.4.0"
}

## Google

provider "google" {
  project = var.project
  region  = var.region
  version = "~> 3.44"
}

provider "google-beta" {
  project = var.project
  region  = var.region
  version = "~> 3.44"
}

## CloudFlare

provider "cloudflare" {
  version    = "= 2.5.1"
  email      = var.cloudflare_email
  api_key    = var.cloudflare_api_key
  account_id = var.cloudflare_account_id
}

##################################
#
#  Service Accounts
#
##################################

# Create the bootstrap KMS key ring
resource "google_kms_key_ring" "bootstrap" {
  name     = "gitlab-${var.environment}-bootstrap"
  location = "global"
  project  = var.project
}

resource "google_kms_key_ring_iam_binding" "bootstrap" {
  key_ring_id = "${var.project}/global/gitlab-${var.environment}-bootstrap"
  role        = "roles/cloudkms.cryptoKeyDecrypter"

  members = [
    "serviceAccount:${var.service_account_email}",
  ]
}

resource "google_kms_crypto_key" "bootstrap-validation" {
  name            = "gitlab-${var.environment}-bootstrap-validation"
  key_ring        = google_kms_key_ring.bootstrap.id
  rotation_period = "7776000s" # 90 days

  lifecycle {
    prevent_destroy = true
  }
}

resource "google_kms_key_ring" "secrets" {
  name     = "gitlab-secrets"
  location = "global"
  project  = var.project
}

resource "google_kms_key_ring_iam_binding" "secrets" {
  key_ring_id = "${var.project}/global/gitlab-secrets"
  role        = "roles/cloudkms.cryptoKeyDecrypter"

  members = [
    "serviceAccount:${var.service_account_email}",
  ]
}

resource "google_kms_crypto_key" "secrets" {
  name            = var.environment
  key_ring        = google_kms_key_ring.secrets.id
  rotation_period = "7776000s" # 90 days

  lifecycle {
    prevent_destroy = true
  }
}

##################################
#
#  Google storage buckets
#
##################################

module "storage" {
  environment                       = var.environment
  service_account_email             = var.service_account_email
  gcs_service_account_email         = var.gcs_service_account_email
  gcs_storage_analytics_group_email = var.gcs_storage_analytics_group_email
  source                            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/storage-buckets.git?ref=v5.13.0"
  project                           = var.project
  prometheus_workload_identity      = false
}

##################################
#
#  Network
#
#################################

module "network" {
  source           = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/vpc.git?ref=v2.0.1"
  project          = var.project
  environment      = var.environment
  internal_subnets = var.internal_subnets
}

##################################
#
#  NAT gateway
#
#################################

module "nat" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/cloud-nat.git?ref=v1.3.0"

  log_level    = "ALL"
  nat_ip_count = 1
  nat_name     = "nat-${var.environment}-${var.region}"
  network_name = module.network.name
  region       = var.region
  router_name  = "nat-router-${var.environment}-${var.region}"
}

##################################
#
#  Bastion
#
##################################

module "bastion" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-bastion]\""
  dns_zone_name         = var.cloudflare_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  health_check          = "http"
  ip_cidr_range         = var.subnetworks["bastion"]
  machine_type          = var.machine_types["bastion"]
  name                  = "bastion"
  node_count            = var.node_count["bastion"]
  project               = var.project
  public_ports          = var.public_ports["bastion"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v6.0.2"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

#### Load balancer for bastion
module "gcp-tcp-lb-bastion" {
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs_bastion["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns_bastion
  gitlab_zone            = "gitlab.com."
  health_check_ports     = var.tcp_lbs_bastion["health_check_ports"]
  instances              = module.bastion.instances_self_link
  lb_count               = length(var.tcp_lbs_bastion["names"])
  name                   = "gcp-tcp-lb-bastion"
  names                  = var.tcp_lbs_bastion["names"]
  project                = var.project
  region                 = var.region
  session_affinity       = "CLIENT_IP"
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v4.1.1"
  targets                = ["bastion"]
}

##################################
#
#  All in one instance
#
##################################

module "all-in-one" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-gitlab]\""
  dns_zone_name         = var.cloudflare_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["all-in-one"]
  machine_type          = var.machine_types["all-in-one"]
  name                  = "omnibus"
  node_count            = var.node_count["all-in-one"]
  os_disk_size          = 100
  project               = var.project
  public_ports          = var.public_ports["all-in-one"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 443
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
  use_external_ip       = true
}

module "db-sharding-poc-dns-record" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab.net."

  a = {
    "db-sharding-poc.gitlab.net." = {
      ttl     = "300"
      records = module.all-in-one.instances.*.network_interface.0.access_config.0.nat_ip
    },
    "registry.db-sharding-poc.gitlab.net." = {
      ttl     = "300"
      records = module.all-in-one.instances.*.network_interface.0.access_config.0.nat_ip
    }
  }
}
