## State storage
terraform {
  backend "s3" {
    bucket = "gitlab-com-infrastructure"
    key    = "terraform/gprd/terraform.tfstate"
    region = "us-east-1"
  }
}

## AWS
provider "aws" {
  region  = "us-east-1"
  version = "~> 2.27.0"
}

## Local
provider "local" {
  version = "~> 1.4.0"
}

## Google

provider "google" {
  project = var.project
  region  = var.region
  version = "~> 3.44"
}

provider "google-beta" {
  project = var.project
  region  = var.region
  version = "~> 3.44"
}

## CloudFlare

provider "cloudflare" {
  version    = "= 2.5.1"
  email      = var.cloudflare_email
  api_key    = var.cloudflare_api_key
  account_id = var.cloudflare_account_id
}

provider "random" {
  version = "~> 2.2"
}

resource "google_project_iam_member" "serviceAccountTokenCreator" {
  project = var.project
  role    = "roles/iam.serviceAccountTokenCreator"
  member  = "serviceAccount:${var.service_account_email}"
}

resource "google_project_iam_member" "serviceAccountUser" {
  project = var.project
  role    = "roles/iam.serviceAccountUser"
  member  = "serviceAccount:${var.service_account_email}"
}

resource "google_project_iam_member" "logging_logWriter" {
  project = var.project
  role    = "roles/logging.logWriter"
  member  = "serviceAccount:${var.service_account_email}"
}

resource "google_project_iam_member" "pubsub_editor" {
  project = var.project
  role    = "roles/pubsub.editor"
  member  = "serviceAccount:${var.service_account_email}"
}

resource "google_project_iam_member" "pubsub_publisher" {
  project = var.project
  role    = "roles/pubsub.publisher"
  member  = "serviceAccount:${var.service_account_email}"
}

resource "google_project_iam_member" "pubsub_subscriber" {
  project = var.project
  role    = "roles/pubsub.subscriber"
  member  = "serviceAccount:${var.service_account_email}"
}

resource "google_compute_firewall" "consul-deny-internal" {
  name    = format("%v-consul-deny-internal", var.environment)
  network = var.environment

  priority  = "500" # must come before any other standard allows (typically priority 1000)
  direction = "INGRESS"

  deny {
    protocol = "tcp"

    ports = [
      "8500",
    ]
  }
}


##################################
#
#  NAT gateway
#
#################################
module "nat" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/cloud-nat.git?ref=v1.3.0"

  log_level        = "ALL"
  nat_ip_count     = 0
  nat_ports_per_vm = 2048
  network_name     = module.network.name
  region           = var.region

  # A NAT instance with this name was initially created for GKE's subnet. Later,
  # it was decided to use 1 Cloud NAT instance per region rather than per
  # subnet, for simplicity. Cloud NAT resources cannot have their name changed
  # without recreation, and 2 NATs cannot overlap in coverage (e.g. 1
  # subnet-specific NAT coexisting with 1 regional NAT). Therefore changing the
  # NAT or router name would cause downtime. This is why in our main
  # environments the Cloud NAT resource is called "gitlab-gke".
  nat_name    = "gitlab-gke"
  router_name = "gitlab-gke"

  # Prevents change in IP name schema in v1.0.0, which would cause downtime.
  ip_name_prefix = "nat-gprd-us-east1"

  imported_ip_prefix      = "address"
  imported_ip_cidr        = "34.74.90.64/28"
  imported_ip_start_index = 0
  imported_ip_count       = 16

  secondary_imported_ip_prefix      = "block"
  secondary_imported_ip_cidr        = "34.74.226.0/24"
  secondary_imported_ip_start_index = 0
  secondary_imported_ip_count       = 16
}

# We want only 1 logs-based metric per project, not per NAT. This way, the
# stackdriver metric (and exported prometheus metric) will have a static name,
# with different NAT instances being identified by the "nat_name" label. Note
# that Stackdriver already applies a "gateway_name" label so we don't need to
# add our own.
resource "google_logging_metric" "errors" {
  name = "nat-errors"

  filter = <<EOS
resource.type="nat_gateway"
AND jsonPayload.allocation_status="DROPPED"
EOS

  # counter
  metric_descriptor {
    metric_kind = "DELTA"
    value_type  = "INT64"
  }
}

resource "google_logging_metric" "errors_by_vm" {
  name = "nat-errors-by-vm"

  filter = <<EOS
resource.type="nat_gateway"
AND jsonPayload.allocation_status="DROPPED"
EOS

  # counter
  metric_descriptor {
    metric_kind = "DELTA"
    value_type  = "INT64"
    labels {
      key        = "vm_name"
      value_type = "STRING"
    }
  }

  label_extractors = {
    "vm_name" = "EXTRACT(jsonPayload.endpoint.vm_name)"
  }
}

resource "google_logging_metric" "translations" {
  name = "nat-translations"

  filter = <<EOS
resource.type="nat_gateway"
AND jsonPayload.allocation_status="OK"
EOS

  # counter
  metric_descriptor {
    metric_kind = "DELTA"
    value_type  = "INT64"
  }
}

##################################
#
#  Network
#
#################################

module "network" {
  environment      = var.environment
  project          = var.project
  source           = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/vpc.git?ref=v2.0.0"
  internal_subnets = var.internal_subnets
}

##################################
#
#  Network Peering
#
#################################

resource "google_compute_network_peering" "peering" {
  count        = length(var.peer_networks["names"])
  name         = "peering-${element(var.peer_networks["names"], count.index)}"
  network      = var.network_env
  peer_network = element(var.peer_networks["links"], count.index)
}

##################################
#
#  Web front-end
#
#################################

module "web" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-fe-web]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.web_egress_ports
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["web"]
  machine_type          = var.machine_types["web"]
  name                  = "web"
  node_count            = var.node_count["web"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["web"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 443
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Deploy Canary
#
##################################

module "deploy-cny" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-deploy-node-cny]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.deploy_egress_ports
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["deploy-cny"]
  machine_type          = var.machine_types["deploy"]
  name                  = "deploy-cny"
  node_count            = var.node_count["deploy-cny"]
  project               = var.project
  public_ports          = var.public_ports["deploy"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "sv"
  use_external_ip       = false
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Web Canary front-end
#
#################################

module "web-cny" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-fe-web-cny]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["web"]
  machine_type          = var.machine_types["web"]
  name                  = "web-cny"
  node_count            = var.node_count["web-cny"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["web"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 443
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  subnetwork_name       = module.web.google_compute_subnetwork_name
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  File canary for gitlab projects
#
##################################

module "file-cny" {
  allow_stopping_for_update = var.allow_stopping_for_update
  assign_public_ip          = false
  bootstrap_version         = var.bootstrap_script_version
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-base-stor-gitaly-cny]\""
  deletion_protection       = true
  data_disk_size            = var.data_disk_sizes["file-cny"]
  data_disk_type            = "pd-ssd"
  dns_zone_name             = var.dns_zone_name
  egress_ports              = var.egress_ports
  environment               = var.environment
  ip_cidr_range             = var.subnetworks["stor-cny"]
  machine_type              = var.machine_types["stor-cny"]
  name                      = "file-cny"
  node_count                = var.node_count["stor-cny"]
  os_disk_type              = "pd-ssd"
  project                   = var.project
  public_ports              = var.public_ports["stor"]
  region                    = var.region
  service_account_email     = var.service_account_email
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v4.2.4"
  tier                      = "stor"
  use_new_node_name         = true
  vpc                       = module.network.self_link
}

##################################
#
#  File servers for marquee projects
#  https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/8650
#
##################################

module "file-marquee" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-stor-gitaly-marquee]\""
  deletion_protection   = true
  data_disk_size        = var.data_disk_sizes["file-marquee"]
  data_disk_type        = "pd-ssd"
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  ip_cidr_range         = var.subnetworks["stor-marquee"]
  machine_type          = var.machine_types["stor-marquee"]
  name                  = "file-marquee"
  node_count            = var.node_count["stor-marquee"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["stor"]
  region                = var.region
  service_account_email = var.service_account_email
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v4.2.4"
  tier                  = "stor"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  File servers to be accessed via praefect
#
##################################

module "file-praefect" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-stor-gitaly-praefect]\""
  deletion_protection   = true
  data_disk_size        = var.data_disk_sizes["file-praefect"]
  data_disk_type        = "pd-ssd"
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  ip_cidr_range         = var.subnetworks["stor-praefect"]
  machine_type          = var.machine_types["stor-praefect"]
  name                  = "file-praefect"
  node_count            = 0 # Make all nodes multi-zone
  multizone_node_count  = var.node_count["stor-praefect"]
  os_boot_image         = var.os_boot_image["file-praefect"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["stor"]
  region                = var.region
  service_account_email = var.service_account_email
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v4.2.4"
  tier                  = "stor"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  File servers on HDDs
#
##################################

module "file-hdd" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-stor-gitaly-hdd]\""
  deletion_protection   = true
  data_disk_size        = var.data_disk_sizes["file-hdd"]
  data_disk_type        = "pd-standard"
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  ip_cidr_range         = var.subnetworks["stor-hdd"]
  machine_type          = var.machine_types["stor-hdd"]
  name                  = "file-hdd"
  node_count            = 0 # Make all nodes multi-zone
  multizone_node_count  = var.node_count["stor-hdd"]
  os_boot_image         = var.os_boot_image["file-hdd"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["stor"]
  region                = var.region
  service_account_email = var.service_account_email
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v4.2.4"
  tier                  = "stor"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Pages web front-end
#
#################################

module "web-pages" {
  // TODO: Public IPs are needed to avoid NAT port exhaustion until the problems
  // described in https://gitlab.com/gitlab-com/gl-infra/production/-/issues/2061#note_335977192
  // are fixed, or we deploy and internal API LB for pages to use.
  assign_public_ip = true

  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-fe-web-pages]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["web-pages"]
  machine_type          = var.machine_types["web-pages"]
  name                  = "web-pages"
  node_count            = var.node_count["web-pages"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["web-pages"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 443
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
  // Zone for pages is set to us-east1-c because the NFS
  // server is also in this zone
  // see https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/10194
  zone = "us-east1-c"
}

##################################
#
#  Database
#
#################################

module "postgres-dr-archive" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_init_run_list    = "\"recipe[gitlab-server::hack_gitlab_ctl_reconfigure]\""
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-db-postgres-archive]\""
  data_disk_size        = var.data_disk_sizes["patroni-v12"]
  data_disk_type        = "pd-ssd"
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  ip_cidr_range         = var.subnetworks["db-dr-archive"]
  machine_type          = var.machine_types["db-dr-archive"]
  name                  = "postgres-dr-archive"
  node_count            = "1"
  project               = var.project
  public_ports          = var.public_ports["db-dr"]
  region                = var.region
  service_account_email = var.service_account_email
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v4.2.4"
  tier                  = "db"
  use_new_node_name     = true
  vpc                   = module.network.self_link
  os_disk_size          = 100
}

module "postgres-dr-delayed" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_init_run_list    = "\"recipe[gitlab-server::hack_gitlab_ctl_reconfigure]\""
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-db-postgres-delayed]\""
  data_disk_size        = var.data_disk_sizes["patroni-v12"]
  data_disk_type        = "pd-ssd"
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  ip_cidr_range         = var.subnetworks["db-dr-delayed"]
  machine_type          = var.machine_types["db-dr-delayed"]
  name                  = "postgres-dr-delayed"
  node_count            = "1"
  project               = var.project
  public_ports          = var.public_ports["db-dr"]
  region                = var.region
  service_account_email = var.service_account_email
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v4.2.4"
  tier                  = "db"
  use_new_node_name     = true
  vpc                   = module.network.self_link
  os_disk_size          = 100
}

module "postgres-backup" {
  environment    = var.environment
  backup_readers = ["serviceAccount:${var.gcs_postgres_restore_service_account}", "serviceAccount:${google_service_account.dr-sa.email}"]
  backup_writers = ["serviceAccount:${var.gcs_postgres_backup_service_account}"]
  kms_key_id     = ""
  source         = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/database-backup-bucket.git?ref=v4.3.0"
}

#############################################
#
#  GCP Internal TCP LoadBalancer and PgBouncer
#
#############################################

module "gcp-tcp-lb-internal-pgbouncer" {
  backend_service        = module.pgbouncer.google_compute_region_backend_service_self_link
  environment            = var.environment
  forwarding_port_ranges = ["6432"]
  fqdns                  = var.lb_fqdns_internal_pgbouncer
  gitlab_zone            = "gitlab.net."
  health_check_ports     = ["6432"]
  instances              = module.pgbouncer.instances_self_link
  lb_count               = "1"
  load_balancing_scheme  = "INTERNAL"
  name                   = "gcp-tcp-lb-internal-pgbouncer"
  names                  = ["${var.environment}-pgbouncer"]
  project                = var.project
  region                 = var.region
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v4.1.1"
  subnetwork_self_link   = module.pgbouncer.google_compute_subnetwork_self_link
  targets                = ["pgbouncer"]
  vpc                    = module.network.self_link
}

module "pgbouncer" {
  assign_public_ip       = false
  backend_service_type   = "regional"
  bootstrap_version      = var.bootstrap_script_version
  chef_init_run_list     = "\"recipe[gitlab-server::hack_gitlab_ctl_reconfigure]\""
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-db-pgbouncer]\""
  create_backend_service = true
  dns_zone_name          = var.dns_zone_name
  environment            = var.environment
  health_check           = "tcp"
  ip_cidr_range          = var.subnetworks["pgbouncer"]
  machine_type           = var.machine_types["pgbouncer"]
  name                   = "pgbouncer"
  node_count             = var.node_count["pgbouncer"]
  project                = var.project
  public_ports           = var.public_ports["pgbouncer"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_port           = 6432
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                   = "db"
  use_new_node_name      = true
  vpc                    = module.network.self_link
}

module "gcp-tcp-lb-internal-pgbouncer-sidekiq" {
  backend_service        = module.pgbouncer-sidekiq.google_compute_region_backend_service_self_link
  environment            = var.environment
  forwarding_port_ranges = ["6432"]
  fqdns                  = var.lb_fqdns_internal_pgbouncer_sidekiq
  gitlab_zone            = "gitlab.net."
  health_check_ports     = ["6432"]
  instances              = module.pgbouncer-sidekiq.instances_self_link
  lb_count               = "1"
  load_balancing_scheme  = "INTERNAL"
  # GCP has a limit of 61 characters over the name the health check, so we had to
  # use "int" instead of "internal" in the name property in order to meet the constraint.
  name                 = "gcp-tcp-lb-int-pgbouncer-sidekiq"
  names                = ["${var.environment}-pgbouncer-sidekiq"]
  project              = var.project
  region               = var.region
  source               = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v4.1.1"
  subnetwork_self_link = module.pgbouncer-sidekiq.google_compute_subnetwork_self_link
  targets              = ["pgbouncer-sidekiq"]
  vpc                  = module.network.self_link
}

module "pgbouncer-sidekiq" {
  assign_public_ip       = false
  backend_service_type   = "regional"
  bootstrap_version      = var.bootstrap_script_version
  chef_init_run_list     = "\"recipe[gitlab-server::hack_gitlab_ctl_reconfigure]\""
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-db-pgbouncer-sidekiq]\""
  create_backend_service = true
  dns_zone_name          = var.dns_zone_name
  environment            = var.environment
  health_check           = "tcp"
  ip_cidr_range          = var.subnetworks["pgbouncer-sidekiq"]
  machine_type           = var.machine_types["pgbouncer"]
  name                   = "pgbouncer-sidekiq"
  node_count             = var.node_count["pgbouncer-sidekiq"]
  project                = var.project
  public_ports           = var.public_ports["pgbouncer"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_port           = 6432
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                   = "db"
  use_new_node_name      = true
  vpc                    = module.network.self_link
}

#############################################
#
#  Patroni
#
#############################################

module "patroni" {
  assign_public_ip       = false
  backend_service_type   = "regional"
  bootstrap_version      = var.bootstrap_script_version
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-db-patroni]\""
  create_backend_service = true
  data_disk_size         = var.data_disk_sizes["patroni"]
  data_disk_type         = "pd-ssd"
  dns_zone_name          = var.dns_zone_name
  environment            = var.environment
  health_check           = "http"
  health_check_port      = "8009"
  log_disk_size          = "250"
  ip_cidr_range          = var.subnetworks["patroni"]
  machine_type           = var.machine_types["patroni"]
  name                   = "patroni"
  node_count             = var.node_count["patroni"]
  project                = var.project
  public_ports           = var.public_ports["patroni"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_path           = "/"
  service_port           = 6432
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor-with-group.git?ref=v4.1.1"
  tier                   = "db"
  use_new_node_name      = true
  vpc                    = module.network.self_link
  os_disk_size           = 100

  per_node_chef_run_list = {
    "7" = "\"role[${var.environment}-base-db-patroni-backup-replica]\""
  }
}

module "patroni-v12" {
  assign_public_ip       = false
  backend_service_type   = "regional"
  bootstrap_version      = var.bootstrap_script_version
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-db-patroni-v12]\""
  create_backend_service = true
  data_disk_size         = var.data_disk_sizes["patroni-v12"]
  data_disk_type         = "pd-ssd"
  dns_zone_name          = var.dns_zone_name
  environment            = var.environment
  health_check           = "http"
  health_check_port      = "8009"
  ip_cidr_range          = var.subnetworks["patroni-v12"]
  machine_type           = var.machine_types["patroni-v12"]
  name                   = "patroni-v12"
  node_count             = var.node_count["patroni-v12"]
  project                = var.project
  public_ports           = var.public_ports["patroni-v12"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_path           = "/"
  service_port           = 6432
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor-with-group.git?ref=v4.1.1"
  tier                   = "db"
  use_new_node_name      = true
  vpc                    = module.network.self_link
  os_disk_size           = 100
  os_boot_image          = var.os_boot_image["patroni-v12"]
  os_disk_type           = var.os_boot_disk_type["patroni-v12"]
  log_disk_type          = var.log_disk_type["patroni-v12"]

  per_node_chef_run_list = {
    "9" = "\"role[${var.environment}-base-db-patroni-backup-replica]\""
  }
}

module "patroni-zfs" {
  assign_public_ip       = false
  bootstrap_data_disk    = false
  bootstrap_version      = 9
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-db-patroni-zfs]\""
  create_backend_service = true
  data_disk_size         = var.data_disk_sizes["patroni-v12"]
  data_disk_type         = "pd-ssd"
  dns_zone_name          = var.dns_zone_name
  environment            = var.environment
  health_check           = "http"
  health_check_port      = "8009"
  log_disk_size          = "250"
  ip_cidr_range          = var.subnetworks["patroni-zfs"]
  machine_type           = var.machine_types["patroni"]
  name                   = "patroni-v12-zfs"
  node_count             = var.node_count["patroni-zfs"]
  project                = var.project
  public_ports           = var.public_ports["patroni"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_path           = "/"
  service_port           = 6432
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor-with-group.git?ref=v4.1.1"
  tier                   = "db"
  use_new_node_name      = true
  vpc                    = module.network.self_link
  os_disk_size           = 100
}

##################################
#
#  Redis
#
##################################

module "redis" {
  assign_public_ip          = false
  allow_stopping_for_update = false
  bootstrap_version         = var.bootstrap_script_version
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-base-db-redis-server-single]\""
  data_disk_size            = 52
  data_disk_type            = "pd-ssd"
  dns_zone_name             = var.dns_zone_name
  egress_ports              = var.egress_ports
  environment               = var.environment
  ip_cidr_range             = var.subnetworks["redis"]
  machine_type              = var.machine_types["redis"]
  name                      = "redis"
  node_count                = var.node_count["redis"]
  project                   = var.project
  public_ports              = var.public_ports["redis"]
  region                    = var.region
  service_account_email     = var.service_account_email
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v4.2.4"
  tier                      = "db"
  use_new_node_name         = true
  vpc                       = module.network.self_link
}

module "redis-sidekiq" {
  assign_public_ip          = false
  allow_stopping_for_update = false
  bootstrap_version         = var.bootstrap_script_version
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-base-db-redis-server-sidekiq]\""
  data_disk_size            = 52
  data_disk_type            = "pd-ssd"
  dns_zone_name             = var.dns_zone_name
  egress_ports              = var.egress_ports
  environment               = var.environment
  ip_cidr_range             = var.subnetworks["redis-sidekiq"]
  machine_type              = var.machine_types["redis-sidekiq"]
  name                      = "redis-sidekiq"
  node_count                = var.node_count["redis-sidekiq"]
  project                   = var.project
  public_ports              = var.public_ports["redis-sidekiq"]
  region                    = var.region
  service_account_email     = var.service_account_email
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v4.2.4"
  tier                      = "db"
  use_new_node_name         = true
  vpc                       = module.network.self_link
}

module "redis-cache" {
  assign_public_ip        = false
  bootstrap_version       = var.bootstrap_script_version
  chef_provision          = var.chef_provision
  dns_zone_name           = var.dns_zone_name
  egress_ports            = var.egress_ports
  environment             = var.environment
  ip_cidr_range           = var.subnetworks["redis-cache"]
  name                    = "redis-cache"
  os_boot_image           = var.os_boot_image["redis-cache"]
  project                 = var.project
  public_ports            = var.public_ports["redis-cache"]
  redis_chef_run_list     = "\"role[${var.environment}-base-db-redis-server-cache]\""
  redis_count             = var.node_count["redis-cache"]
  redis_data_disk_size    = 500
  redis_data_disk_type    = "pd-ssd"
  redis_machine_type      = var.machine_types["redis-cache"]
  region                  = var.region
  sentinel_chef_run_list  = "\"role[${var.environment}-base-db-redis-sentinel-cache]\""
  sentinel_count          = var.node_count["redis-cache-sentinel"]
  sentinel_data_disk_size = 100
  sentinel_data_disk_type = "pd-ssd"
  sentinel_machine_type   = var.machine_types["redis-cache-sentinel"]
  service_account_email   = var.service_account_email
  source                  = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor-redis.git?ref=v3.1.1"
  tier                    = "db"
  use_new_node_name       = true
  vpc                     = module.network.self_link
}

module "redis-tracechunks" {
  assign_public_ip          = false
  allow_stopping_for_update = false
  bootstrap_version         = var.bootstrap_script_version
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-base-db-redis-server-tracechunks]\""
  data_disk_size            = 52
  data_disk_type            = "pd-ssd"
  dns_zone_name             = var.dns_zone_name
  egress_ports              = var.egress_ports
  environment               = var.environment
  ip_cidr_range             = var.subnetworks["redis-tracechunks"]
  machine_type              = var.machine_types["redis-tracechunks"]
  name                      = "redis-tracechunks"
  node_count                = var.node_count["redis-tracechunks"]
  os_boot_image             = var.os_boot_image["redis-tracechunks"]
  project                   = var.project
  public_ports              = var.public_ports["redis-tracechunks"]
  region                    = var.region
  service_account_email     = var.service_account_email
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v4.1.0"
  tier                      = "db"
  use_new_node_name         = true
  vpc                       = module.network.self_link
}

##################################
#
#  Sidekiq
#
##################################

module "sidekiq" {
  assign_public_ip                            = false
  allow_stopping_for_update                   = false
  bootstrap_version                           = var.bootstrap_script_version
  chef_provision                              = var.chef_provision
  chef_run_list                               = "\"role[${var.environment}-base-be-sidekiq-catchall]\""
  dns_zone_name                               = var.dns_zone_name
  environment                                 = var.environment
  ip_cidr_range                               = var.subnetworks["sidekiq"]
  machine_type                                = var.machine_types["sidekiq-catchall"]
  name                                        = "sidekiq"
  os_disk_type                                = "pd-ssd"
  project                                     = var.project
  public_ports                                = var.public_ports["sidekiq"]
  region                                      = var.region
  service_account_email                       = var.service_account_email
  sidekiq_elasticsearch_count                 = var.node_count["sidekiq-elasticsearch"]
  sidekiq_elasticsearch_instance_type         = var.machine_types["sidekiq-elasticsearch"]
  sidekiq_low_urgency_cpu_bound_count         = var.node_count["sidekiq-low-urgency-cpu-bound"]
  sidekiq_low_urgency_cpu_bound_instance_type = var.machine_types["sidekiq-low-urgency-cpu-bound"]
  sidekiq_catchall_count                      = var.node_count["sidekiq-catchall"]
  sidekiq_catchall_instance_type              = var.machine_types["sidekiq-catchall"]
  sidekiq_catchall_os_disk_size               = 100
  sidekiq_urgent_other_count                  = var.node_count["sidekiq-urgent-other"]
  sidekiq_urgent_other_instance_type          = var.machine_types["sidekiq-urgent-other"]
  sidekiq_urgent_cpu_bound_count              = var.node_count["sidekiq-urgent-cpu-bound"]
  sidekiq_urgent_cpu_bound_instance_type      = var.machine_types["sidekiq-urgent-cpu-bound"]
  source                                      = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-sidekiq.git?ref=v8.1.1"
  tier                                        = "sv"
  use_new_node_name                           = true
  vpc                                         = module.network.self_link
}

##################################
#
#  Gitaly Praefect nodes
#
##################################

module "praefect" {
  assign_public_ip       = false
  backend_service_type   = "regional"
  bootstrap_version      = var.bootstrap_script_version
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-stor-praefect]\""
  create_backend_service = true
  dns_zone_name          = var.dns_zone_name
  egress_ports           = var.egress_ports
  environment            = var.environment
  health_check           = "tcp"
  health_check_port      = 9652
  ip_cidr_range          = var.subnetworks["praefect"]
  machine_type           = var.machine_types["praefect"]
  name                   = "praefect"
  node_count             = var.node_count["praefect"]
  project                = var.project
  public_ports           = var.public_ports["praefect"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_path           = "/metrics"
  service_port           = 2305
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                   = "stor"
  use_new_node_name      = true
  vpc                    = module.network.self_link
}

module "praefect-db" {
  availability_type               = "REGIONAL" # REGIONAL enables HA
  database_version                = "POSTGRES_11"
  maintenance_window_day          = 6  # Saturday
  maintenance_window_hour         = 11 # UTC
  maintenance_window_update_track = "stable"
  name                            = "praefect-db"
  project                         = var.project
  read_replica_disk_autoresize    = false
  read_replica_size               = 1
  read_replica_tier               = var.machine_types["praefect-db"]
  read_replica_zones              = "b,c,d"
  region                          = var.region
  source                          = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/cloud-sql.git?ref=v2.3.0"
  tier                            = var.machine_types["praefect-db"]
  # Note: Network peering can be moody. If your `apply` fails to create the
  # `private_vpc_connection` you can try resetting it:
  # gcloud services vpc-peerings update --service=servicenetworking.googleapis.com --ranges=private-ip --network=pre --project=gitlab-pre --force
  # See https://github.com/terraform-providers/terraform-provider-google/issues/3294#issuecomment-476715149
  vpc = module.network.self_link

  ip_configuration = {
    private_network = module.network.self_link
  }

  read_replica_ip_configuration = {
    private_network = module.network.self_link
  }

  backup_configuration = {
    enabled = "true"
  }

  database_flags = [
    {
      name  = "log_min_duration_statement"
      value = 1000
    },
  ]
}

##################################
#
#  Storage nodes for repositories
#
##################################

module "file" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-stor-gitaly]\""
  deletion_protection   = true
  data_disk_size        = var.data_disk_sizes["file"]
  data_disk_type        = "pd-ssd"
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  ip_cidr_range         = var.subnetworks["stor"]
  machine_type          = var.machine_types["stor"]
  name                  = "file"
  node_count            = var.node_count["stor"]
  multizone_node_count  = var.node_count["multizone-stor"]
  os_boot_image         = var.os_boot_image["file"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["stor"]
  region                = var.region
  service_account_email = var.service_account_email
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v4.2.4"
  tier                  = "stor"
  use_new_node_name     = true
  vpc                   = module.network.self_link
  zone                  = "us-east1-c"
}

##################################
#
#  External HAProxy LoadBalancer
#
##################################

module "fe-lb" {
  assign_public_ip       = false
  backend_service_type   = "regional"
  bootstrap_version      = var.bootstrap_script_version
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-lb-fe]\""
  create_backend_service = true
  dns_zone_name          = var.dns_zone_name
  environment            = var.environment
  health_check           = "http"
  ip_cidr_range          = var.subnetworks["fe-lb"]
  machine_type           = var.machine_types["fe-lb"]
  name                   = "fe"
  node_count             = var.node_count["fe-lb"]
  os_boot_image          = var.os_boot_image["fe-lb"]
  project                = var.project
  public_ports           = var.public_ports["fe-lb"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_path           = "/-/available-https"
  service_port           = 8002
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                   = "lb"
  use_new_node_name      = true
  vpc                    = module.network.self_link
}

##################################
#
#  External HAProxy LoadBalancer Pages
#
##################################

module "fe-lb-pages" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-lb-pages]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "http"
  ip_cidr_range         = var.subnetworks["fe-lb-pages"]
  machine_type          = var.machine_types["fe-lb-pages"]
  name                  = "fe-pages"
  node_count            = var.node_count["fe-lb-pages"]
  os_boot_image         = var.os_boot_image["fe-lb"]
  project               = var.project
  public_ports          = var.public_ports["fe-lb"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 7331
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v5.1.0"
  tier                  = "lb"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  External HAProxy LoadBalancer Registry
#
##################################

module "fe-lb-registry" {
  assign_public_ip       = false
  bootstrap_version      = var.bootstrap_script_version
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-lb-registry]\""
  create_backend_service = true
  dns_zone_name          = var.dns_zone_name
  environment            = var.environment
  health_check           = "http"
  ip_cidr_range          = var.subnetworks["fe-lb-registry"]
  machine_type           = var.machine_types["fe-lb-registry"]
  name                   = "fe-registry"
  node_count             = var.node_count["fe-lb-registry"]
  os_boot_image          = var.os_boot_image["fe-lb"]
  project                = var.project
  public_ports           = var.public_ports["fe-lb"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_path           = "/-/available-https"
  service_port           = 8002
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v5.1.0"
  tier                   = "lb"
  use_new_node_name      = true
  vpc                    = module.network.self_link
}

##################################
#
#  External HAProxy LoadBalancer Canary
#
##################################

module "fe-lb-cny" {
  assign_public_ip       = false
  bootstrap_version      = var.bootstrap_script_version
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-lb-cny]\""
  create_backend_service = true
  dns_zone_name          = var.dns_zone_name
  environment            = var.environment
  health_check           = "http"
  ip_cidr_range          = var.subnetworks["fe-lb-cny"]
  machine_type           = var.machine_types["fe-lb"]
  name                   = "fe-cny"
  node_count             = var.node_count["fe-lb-cny"]
  project                = var.project
  public_ports           = var.public_ports["fe-lb"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_path           = "/-/available-https"
  service_port           = 8002
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                   = "lb"
  use_new_node_name      = true
  vpc                    = module.network.self_link
}

##################################
#
#  GCP TCP LoadBalancers
#
##################################

#### Load balancer for the main site
# Origin for CloudFlare Spectrum
# This replaced the `gcp-tcp-lb`.
# See https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/12434
module "gcp-tcp-lb-spectrum" {
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns
  gitlab_zone            = "gitlab.com."
  health_check_ports     = var.tcp_lbs["health_check_ports"]
  instances              = module.fe-lb.instances_self_link
  lb_count               = length(var.tcp_lbs["names"])
  name                   = "gcp-tcp-lb-spectrum"
  names                  = var.tcp_lbs["names"]
  project                = var.project
  region                 = var.region
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v4.1.1"
  targets                = ["fe"]
  spectrum_config = {
    "proxy_protocol" = "v2",
    "ports" = {
      "22"  = "direct",
      "80"  = "http",
      "443" = "https"
    }
  }
}

##################################
#
#  GCP Internal TCP LoadBalancers
#
##################################

###### Internal Load balancer for the main site
module "gcp-tcp-lb-internal" {
  backend_service        = module.fe-lb.google_compute_region_backend_service_self_link
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs_internal["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns_internal
  gitlab_zone            = "gitlab.net."
  health_check_ports     = var.tcp_lbs_internal["health_check_ports"]
  instances              = module.fe-lb.instances_self_link
  lb_count               = length(var.tcp_lbs_internal["names"])
  load_balancing_scheme  = "INTERNAL"
  name                   = "gcp-tcp-lb-internal"
  names                  = var.tcp_lbs_internal["names"]
  project                = var.project
  region                 = var.region
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v4.1.1"
  subnetwork_self_link   = module.fe-lb.google_compute_subnetwork_self_link
  targets                = ["fe"]
  vpc                    = module.network.self_link
}

#### Load balancer for praefect
module "gcp-tcp-lb-internal-praefect" {
  backend_service            = module.praefect.google_compute_region_backend_service_self_link
  environment                = var.environment
  forwarding_port_ranges     = var.tcp_lbs_internal_praefect["forwarding_port_ranges"]
  fqdns                      = var.lb_fqdns_internal_praefect
  gitlab_zone                = "gitlab.com."
  health_check_ports         = var.tcp_lbs_internal_praefect["health_check_ports"]
  health_check_request_paths = var.tcp_lbs_internal_praefect["health_check_request_paths"]
  instances                  = module.praefect.instances_self_link
  lb_count                   = length(var.tcp_lbs_internal_praefect["names"])
  load_balancing_scheme      = "INTERNAL"
  name                       = "gcp-tcp-lb-internal-praefect"
  names                      = var.tcp_lbs_internal_praefect["names"]
  project                    = var.project
  region                     = var.region
  source                     = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v4.1.1"
  subnetwork_self_link       = module.praefect.google_compute_subnetwork_self_link
  targets                    = ["praefect"]
  vpc                        = module.network.self_link
}

#### Load balancer for pages
module "gcp-tcp-lb-pages" {
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs_pages["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns_pages
  gitlab_zone            = "gitlab.io."
  health_check_ports     = var.tcp_lbs_pages["health_check_ports"]
  instances              = module.fe-lb-pages.instances_self_link
  lb_count               = length(var.tcp_lbs_pages["names"])
  name                   = "gcp-tcp-lb-pages"
  names                  = var.tcp_lbs_pages["names"]
  project                = var.project
  region                 = var.region
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v4.1.1"
  targets                = ["fe-pages"]
}

module "gprd-dns-record" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab.com."

  cname = {
    for k, v in var.lb_fqdns_cny : "${v}" => {
      ttl     = "300"
      records = var.lb_fqdns
    }
  }
  a = {
    ##
    # This defines the spectrum app for altssh.
    # We re-map traffic in Cloudflare. Traffic on port 443 will be proxied to
    # port 22 on the regular load balancers that handle SSH traffic
    for k, v in var.lb_fqdns_altssh : "${v}" => {
      ttl     = "300"
      records = [module.gcp-tcp-lb-spectrum.address]
      spectrum_config = {
        "proxy_protocol" = "v2",
        "ports" = {
          "443" = "22",
        }
      }
    }
  }
}

#### Load balancer for registry
module "gcp-tcp-lb-registry" {
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs_registry["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns_registry
  gitlab_zone            = "gitlab.com."
  health_check_ports     = var.tcp_lbs_registry["health_check_ports"]
  instances              = module.fe-lb-registry.instances_self_link
  lb_count               = length(var.tcp_lbs_registry["names"])
  name                   = "gcp-tcp-lb-registry"
  names                  = var.tcp_lbs_registry["names"]
  project                = var.project
  region                 = var.region
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v4.1.1"
  targets                = ["fe-registry"]
}

#### Load balancer for cny
module "gcp-tcp-lb-cny" {
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs_cny["forwarding_port_ranges"]
  fqdns                  = []
  gitlab_zone            = "gitlab.com."
  health_check_ports     = var.tcp_lbs_cny["health_check_ports"]
  instances              = module.fe-lb-cny.instances_self_link
  lb_count               = length(var.tcp_lbs_cny["names"])
  name                   = "gcp-tcp-lb-cny"
  names                  = var.tcp_lbs_cny["names"]
  project                = var.project
  region                 = var.region
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v4.1.1"
  targets                = ["fe-cny"]
}

#### Load balancer for bastion
module "gcp-tcp-lb-bastion" {
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs_bastion["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns_bastion
  gitlab_zone            = "gitlab.com."
  health_check_ports     = var.tcp_lbs_bastion["health_check_ports"]
  instances              = module.bastion.instances_self_link
  lb_count               = length(var.tcp_lbs_bastion["names"])
  name                   = "gcp-tcp-lb-bastion"
  names                  = var.tcp_lbs_bastion["names"]
  project                = var.project
  region                 = var.region
  session_affinity       = "CLIENT_IP"
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v4.1.1"
  targets                = ["bastion"]
}

#### Load balancer for teleport
module "gcp-tcp-lb-teleport" {
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs_teleport["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns_teleport
  gitlab_zone            = "gitlab.com."
  health_check_ports     = var.tcp_lbs_teleport["health_check_ports"]
  instances              = module.teleport.instances_self_link
  lb_count               = length(var.tcp_lbs_teleport["names"])
  name                   = "gcp-tcp-lb-teleport"
  names                  = var.tcp_lbs_teleport["names"]
  project                = var.project
  region                 = var.region
  session_affinity       = "CLIENT_IP"
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v4.1.1"
  targets                = ["teleport"]
}

##################################
#
#  Consul
#
##################################

module "consul" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-infra-consul]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  ip_cidr_range         = var.subnetworks["consul"]
  machine_type          = var.machine_types["consul"]
  name                  = "consul"
  node_count            = var.node_count["consul"]
  project               = var.project
  public_ports          = var.public_ports["consul"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 8300
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
# Pipeline Verification Service
# Stackdriver Exporter
#
##################################

# GCP Service Account
resource "google_service_account" "pvs-k8s-sa" {
  account_id   = "pvs-k8s"
  display_name = "pvs-k8s"
  description  = "Service account for StackDriver Exporter"
}

resource "google_service_account_iam_member" "pvs-pvs" {
  role               = "roles/iam.workloadIdentityUser"
  service_account_id = google_service_account.pvs-k8s-sa.name
  member             = "serviceAccount:${var.project}.svc.id.goog[monitoring/pvs-stackdriver-exporter]"
}

##################################
#
#  PubSub
#
##################################

module "pubsub" {
  source                            = "../../modules/pubsub"
  environment                       = var.environment
  project                           = var.project
  pubsub_topics                     = var.pubsub_topics
  pubsub_filters                    = var.pubsub_filters
  service_account_email             = var.service_account_email
  additional_pubsub_publisher_roles = var.additional_pubsub_publisher_roles

}

##################################
#
#  Pubsubbeats
#
##################################

## GKE Workload Identity config for beats in kubernetes

# Create a GCP service account
resource "google_service_account" "pubsubbeat-k8s-sa" {
  account_id   = "pubsubbeat-k8s"
  display_name = "pubsubbeat-k8s"
  description  = "Service account used by pubsubbeat in k8s for subscribing to PubSub"
}

# Bind a k8s service account pubsubbeat/es-diagnostics to a GCP service account
resource "google_service_account_iam_member" "pubsubbeat-pubsubbeat" {
  role               = "roles/iam.workloadIdentityUser"
  service_account_id = google_service_account.pubsubbeat-k8s-sa.name
  member             = "serviceAccount:${var.project}.svc.id.goog[pubsubbeat/pubsubbeat]"
}

# Give the GCP service account relevant PubSub permissions (assign roles)
resource "google_project_iam_member" "pubsubbeat-pubsubbeat-pubsub-subscriber" {
  project = var.project
  role    = "roles/pubsub.subscriber"
  member  = "serviceAccount:${google_service_account.pubsubbeat-k8s-sa.email}"
}

resource "google_project_iam_member" "pubsubbeat-pubsubbeat-pubsub-editor" {
  project = var.project
  role    = "roles/pubsub.editor"
  member  = "serviceAccount:${google_service_account.pubsubbeat-k8s-sa.email}"
}

resource "google_service_account" "windows-ci-manager-pubsub" {
  account_id   = "windows-pubsub"
  display_name = "Windows runner managers pubsub service account"
  description  = "Service account used by the windows runners manager"
}

resource "google_project_iam_custom_role" "pubsub-permissions" {
  role_id     = "PubsubPermissions"
  title       = "Pubsub permissions"
  permissions = ["pubsub.topics.get", "pubsub.topics.publish"]
}

resource "google_project_iam_member" "windows-ci-manager-logging" {
  role   = google_project_iam_custom_role.pubsub-permissions.id
  member = "serviceAccount:${google_service_account.windows-ci-manager-pubsub.email}"
}

##################################
#
#  Monitoring
#
#  Uses the monitoring module, this
#  creates a single instance behind
#  a load balancer with identity aware
#  proxy enabled.
#
##################################

resource "google_compute_subnetwork" "monitoring" {
  ip_cidr_range            = var.subnetworks["monitoring"]
  name                     = format("monitoring-%v", var.environment)
  network                  = module.network.self_link
  private_ip_google_access = true
  project                  = var.project
  region                   = var.region
}

#######################
#
# load balancer for all hosts in this section
#
#######################

module "monitoring-lb" {
  environment     = var.environment
  dns_zone_id     = var.gitlab_net_zone_id
  dns_zone_name   = "gitlab.net"
  dns_zone_prefix = "gprd."
  hosts           = var.monitoring_hosts["names"]
  name            = "monitoring-lb"
  project         = var.project
  region          = var.region
  service_ports   = var.monitoring_hosts["ports"]
  source          = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/https-lb.git?ref=v3.0.1"
  subnetwork_name = google_compute_subnetwork.monitoring.name
  targets         = var.monitoring_hosts["names"]
  url_map         = google_compute_url_map.monitoring-lb.self_link
}

#######################
module "prometheus" {
  assign_public_ip  = true
  bootstrap_version = var.bootstrap_script_version
  chef_provision    = var.chef_provision
  chef_run_list     = "\"role[${var.environment}-infra-prometheus]\""
  data_disk_size    = var.data_disk_sizes["prometheus"]
  data_disk_type    = "pd-standard"
  dns_zone_name     = var.dns_zone_name
  environment       = var.environment
  fw_whitelist_subnets = concat(
    var.monitoring_whitelist_prometheus["subnets"],
    var.other_monitoring_subnets,
  )
  fw_whitelist_ports    = var.monitoring_whitelist_prometheus["ports"]
  machine_type          = var.machine_types["monitoring-default"]
  name                  = "prometheus"
  node_count            = var.node_count["prometheus"]
  oauth2_client_id      = var.oauth2_client_id_monitoring
  oauth2_client_secret  = var.oauth2_client_secret_monitoring
  persistent_disk_path  = "/opt/prometheus"
  project               = var.project
  region                = var.region
  service_account_email = var.service_account_email
  service_ports = [
    {
      "name" : "prometheus",
      "port" : element(
        var.monitoring_hosts["ports"],
        index(var.monitoring_hosts["names"], "prometheus"),
      ),
      "health_check_path" : "/graph",
    },
  ]
  source            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v5.0.0"
  subnetwork_name   = google_compute_subnetwork.monitoring.name
  tier              = "inf"
  use_external_ip   = true
  use_new_node_name = true
  vpc               = module.network.self_link
}

module "prometheus-app" {
  assign_public_ip  = false
  bootstrap_version = var.bootstrap_script_version
  chef_provision    = var.chef_provision
  chef_run_list     = "\"role[${var.environment}-infra-prometheus-app]\""
  data_disk_size    = var.data_disk_sizes["prometheus"]
  data_disk_type    = "pd-standard"
  dns_zone_name     = var.dns_zone_name
  environment       = var.environment
  fw_whitelist_subnets = concat(
    var.monitoring_whitelist_prometheus["subnets"],
    var.other_monitoring_subnets,
  )
  fw_whitelist_ports    = var.monitoring_whitelist_prometheus["ports"]
  machine_type          = var.machine_types["monitoring-app"]
  name                  = "prometheus-app"
  node_count            = var.node_count["prometheus-app"]
  oauth2_client_id      = var.oauth2_client_id_monitoring
  oauth2_client_secret  = var.oauth2_client_secret_monitoring
  persistent_disk_path  = "/opt/prometheus"
  project               = var.project
  region                = var.region
  service_account_email = var.service_account_email
  service_ports = [
    {
      "name" : "prometheus-app",
      "port" : element(
        var.monitoring_hosts["ports"],
        index(var.monitoring_hosts["names"], "prometheus-app"),
      ),
      "health_check_path" : "/graph",
    },
  ]
  source            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v5.0.0"
  subnetwork_name   = google_compute_subnetwork.monitoring.name
  tier              = "inf"
  use_external_ip   = false
  use_new_node_name = true
  vpc               = module.network.self_link
}

module "prometheus-db" {
  assign_public_ip  = false
  bootstrap_version = var.bootstrap_script_version
  chef_provision    = var.chef_provision
  chef_run_list     = "\"role[${var.environment}-infra-prometheus-db]\""
  data_disk_size    = var.data_disk_sizes["prometheus-db"]
  data_disk_type    = "pd-standard"
  dns_zone_name     = var.dns_zone_name
  environment       = var.environment
  fw_whitelist_subnets = concat(
    var.monitoring_whitelist_prometheus["subnets"],
    var.other_monitoring_subnets,
  )
  fw_whitelist_ports    = var.monitoring_whitelist_prometheus["ports"]
  machine_type          = var.machine_types["monitoring-db"]
  name                  = "prometheus-db"
  node_count            = var.node_count["prometheus-db"]
  oauth2_client_id      = var.oauth2_client_id_monitoring
  oauth2_client_secret  = var.oauth2_client_secret_monitoring
  persistent_disk_path  = "/opt/prometheus"
  project               = var.project
  region                = var.region
  service_account_email = var.service_account_email
  service_ports = [
    {
      "name" : "prometheus-db",
      "port" : element(
        var.monitoring_hosts["ports"],
        index(var.monitoring_hosts["names"], "prometheus-db"),
      ),
      "health_check_path" : "/graph",
    },
  ]
  source            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v5.0.0"
  subnetwork_name   = google_compute_subnetwork.monitoring.name
  tier              = "inf"
  use_external_ip   = false
  use_new_node_name = true
  vpc               = module.network.self_link
}

module "sd-exporter" {
  assign_public_ip          = false
  additional_scopes         = ["https://www.googleapis.com/auth/monitoring"]
  allow_stopping_for_update = false
  bootstrap_version         = var.bootstrap_script_version
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-infra-sd-exporter]\""
  create_backend_service    = false
  dns_zone_name             = var.dns_zone_name
  environment               = var.environment
  machine_type              = var.machine_types["sd-exporter"]
  name                      = "sd-exporter"
  node_count                = var.node_count["sd-exporter"]
  project                   = var.project
  public_ports              = var.public_ports["sd-exporter"]
  region                    = var.region
  service_account_email     = var.service_account_email
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  subnetwork_name           = google_compute_subnetwork.monitoring.name
  tier                      = "inf"
  use_new_node_name         = true
  vpc                       = module.network.self_link
}

module "blackbox" {
  assign_public_ip      = true
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-blackbox]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  machine_type          = var.machine_types["blackbox"]
  name                  = "blackbox"
  node_count            = var.node_count["blackbox"]
  project               = var.project
  public_ports          = var.public_ports["blackbox"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  subnetwork_name       = google_compute_subnetwork.monitoring.name
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
  use_external_ip       = true
}

#### Allow pushgateway access from Ops VMs

resource "google_compute_firewall" "blackbox-allow-pushgateway-ops" {
  name    = format("%v-blackbox-allow-pushgateway", var.environment)
  network = var.environment

  priority  = "1000"
  direction = "INGRESS"

  allow {
    protocol = "tcp"
    ports    = ["9091"]
  }

  source_ranges = [
    "10.250.17.0/24", # runner-snapshots
    "10.250.10.0/24", # runner-chatops
    "10.250.24.0/24", # console
  ]

  target_tags = ["blackbox"]
}

module "thanos-compact" {
  assign_public_ip          = false
  allow_stopping_for_update = false
  bootstrap_version         = var.bootstrap_script_version
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-infra-thanos-compact]\""
  data_disk_size            = 1500
  data_disk_type            = "pd-ssd"
  dns_zone_name             = var.dns_zone_name
  egress_ports              = var.egress_ports
  environment               = var.environment
  ip_cidr_range             = var.subnetworks["thanos-compact"]
  machine_type              = var.machine_types["thanos-compact"]
  monitoring_whitelist      = var.monitoring_whitelist_thanos
  name                      = "thanos-compact"
  node_count                = var.node_count["thanos-compact"]
  persistent_disk_path      = "/opt/prometheus"
  project                   = var.project
  public_ports              = var.public_ports["thanos"]
  region                    = var.region
  service_account_email     = var.service_account_email
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v4.2.4"
  tier                      = "inf"
  use_new_node_name         = true
  vpc                       = module.network.self_link
}

##################################
#
#  Console
#
##################################

module "console" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-console-node]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.console_egress_ports
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["console"]
  machine_type          = var.machine_types["console"]
  name                  = "console"
  node_count            = var.node_count["console"]
  os_disk_size          = 200
  project               = var.project
  public_ports          = var.public_ports["console"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "sv"
  use_external_ip       = true
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

module "console-ro" {
  assign_public_ip      = true
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-console-ro-node]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.console_egress_ports
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["console-ro"]
  machine_type          = var.machine_types["console"]
  name                  = "console-ro"
  node_count            = var.node_count["console-ro"]
  os_disk_size          = 50
  project               = var.project
  public_ports          = var.public_ports["console-ro"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v5.1.3"
  tier                  = "sv"
  use_external_ip       = false
  use_new_node_name     = true
  os_boot_image         = "ubuntu-os-cloud/ubuntu-2004-lts"
  vpc                   = module.network.self_link
}

##################################
#
#  Deploy
#
##################################

module "deploy" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-deploy-node]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.deploy_egress_ports
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["deploy"]
  machine_type          = var.machine_types["deploy"]
  name                  = "deploy"
  node_count            = var.node_count["deploy"]
  project               = var.project
  public_ports          = var.public_ports["deploy"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "sv"
  use_external_ip       = false
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Runner
#
##################################

module "runner" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-runner]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["runner"]
  machine_type          = var.machine_types["runner"]
  name                  = "runner"
  node_count            = var.node_count["runner"]
  project               = var.project
  public_ports          = var.public_ports["runner"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Bastion
#
##################################

module "bastion" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-bastion]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  health_check          = "http"
  ip_cidr_range         = var.subnetworks["bastion"]
  machine_type          = var.machine_types["bastion"]
  name                  = "bastion"
  node_count            = var.node_count["bastion"]
  project               = var.project
  public_ports          = var.public_ports["bastion"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Teleport Bastion
#
##################################

module "teleport" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-bastion-teleport]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  health_check          = "http"
  ip_cidr_range         = var.subnetworks["teleport"]
  machine_type          = var.machine_types["teleport"]
  name                  = "teleport"
  node_count            = var.node_count["teleport"]
  project               = var.project
  public_ports          = var.public_ports["teleport"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 3080
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Object storage buckets
#
##################################

module "gitlab_object_storage" {
  environment                       = var.environment
  service_account_email             = var.service_account_email
  gcs_service_account_email         = var.gcs_service_account_email
  gcs_teleport_account_email        = var.gcs_teleport_session_service_account
  gcs_storage_analytics_group_email = var.gcs_storage_analytics_group_email
  source                            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/storage-buckets.git?ref=v5.15.3"
  secrets_viewer_access = [
    "serviceAccount:${google_service_account.k8s-workloads-ro.email}",
    "serviceAccount:${google_service_account.k8s-workloads.email}"
  ]
  archived_logs_viewer_access = [
    "user:vnagy@gitlab.com",
  ]
  project = var.project

  # note: at time of writing, terraform does not propagate this properly.
  # if you change this value, you may get an empty diff. if so, please update
  # the value in GCP manually.
  registry_days_since_noncurrent_time = 7
}

resource "google_kms_key_ring_iam_binding" "bootstrap" {
  key_ring_id = "${var.project}/global/gitlab-${var.environment}-bootstrap"
  role        = "roles/cloudkms.cryptoKeyDecrypter"

  members = [
    "serviceAccount:${var.service_account_email}",
  ]
}

resource "google_kms_key_ring_iam_binding" "secrets" {
  key_ring_id = "${var.project}/global/gitlab-secrets"
  role        = "roles/cloudkms.cryptoKeyDecrypter"

  members = [
    "serviceAccount:${var.service_account_email}",
  ]
}

############################
# Stackdriver log exclusions
############################

module "stackdriver" {
  sd_log_filters = var.sd_log_filters
  source         = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/stackdriver.git?ref=v2.0.0"
}

############################
# Camo proxy
############################

module "camoproxy-lb" {
  dns_zone_id     = var.gitlab_static_net_zone_id
  dns_zone_name   = var.camoproxy_domain
  environment     = var.environment
  hosts           = [var.camoproxy_hostname]
  name            = "camo-proxy"
  project         = var.project
  region          = var.region
  service_ports   = [var.camoproxy_serviceport]
  source          = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/https-lb.git?ref=v3.0.1"
  subnetwork_name = module.camoproxy.google_compute_subnetwork_name
  targets         = ["camoproxy"]
  url_map         = google_compute_url_map.camoproxy-lb.self_link
}

module "camoproxy" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-svc-camoproxy]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.camoproxy_egress_ports
  environment           = var.environment
  health_check          = "http"
  ip_cidr_range         = var.subnetworks["camoproxy"]
  log_disk_size         = 20
  machine_type          = var.machine_types["camoproxy"]
  name                  = "camoproxy"
  node_count            = var.node_count["camoproxy"]
  os_boot_image         = var.os_boot_image["camoproxy"]
  project               = var.project
  region                = var.region
  service_account_email = var.service_account_email
  service_path          = "/healthcheck"
  service_port          = var.camoproxy_serviceport
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

# Deny all egress to RFC1918 (internal) IP addresses.  The camo proxy must not be able to go direct to any other internal places
resource "google_compute_firewall" "camoproxy-deny-internal-egress" {
  name    = format("%v-camoproxy-deny-internal", var.environment)
  network = var.environment

  priority  = "500" # must come before any other standard allows (typically priority 1000)
  direction = "EGRESS"

  deny {
    protocol = "all"
  }

  destination_ranges = ["10.0.0.0/8", "172.16.0.0/12", "192.168.0.0/16"]

  target_tags = ["camoproxy"]
}

#Simple url map; everything goes to the one backend
resource "google_compute_url_map" "camoproxy-lb" {
  name            = format("%v-camoproxy-lb", var.environment)
  default_service = module.camoproxy.google_compute_backend_service_self_link
}

############
# cloudflare
############

resource "google_storage_bucket" "gprd_cloudflare_logs" {
  name          = "gitlab-gprd-cloudflare-logpush"
  location      = "US"
  project       = var.project
  force_destroy = true

  labels = {
    tfmanaged = "yes"
    name      = "gitlab-gprd-cloudflare-logpush"
  }

  lifecycle_rule {
    condition {
      age = "91"
    }
    action {
      type = "Delete"
    }
  }
}

resource "google_storage_bucket_iam_member" "logpush_cloudflare" {
  bucket = google_storage_bucket.gprd_cloudflare_logs.name
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:logpush@cloudflare-data.iam.gserviceaccount.com"
}

resource "cloudflare_zone_settings_override" "gitlab_com_settings_override" {

  zone_id = var.cloudflare_zone_id
  settings {
    waf                         = "on"
    always_use_https            = "on"
    min_tls_version             = "1.2"
    ssl                         = "origin_pull"
    tls_1_3                     = "on"
    brotli                      = "on"
    sort_query_string_for_cache = "on"
    http2                       = "on"
    ipv6                        = "on"
    websockets                  = "on"
    opportunistic_onion         = "on"
    pseudo_ipv4                 = "off"
    email_obfuscation           = "off"
  }
}

resource "null_resource" "manual_cloudflare_settings" {

  provisioner "local-exec" {
    command = <<EOF
      echo '
      # 2020-03-23 Contacted cloudflare to increase limit to 5GiB https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/9167
      # 2020-03-13 Contacted cloudflare to increase timeout to 400 seconds https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/8475'
      EOF
  }

}

resource "cloudflare_argo" "gitlab_com_argo" {
  zone_id        = var.cloudflare_zone_id
  tiered_caching = "on"
  smart_routing  = "on"
}

module "cloudflare_workers" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/cloudflare_workers.git?ref=v1.0.5"

  cloudflare_zone_name = var.cloudflare_zone_name
  cloudflare_zone_id   = var.cloudflare_zone_id
  environment          = var.environment
}

resource "cloudflare_custom_pages" "basic_challenge" {
  account_id = var.cloudflare_account_id
  type       = "basic_challenge"
  url        = "https://gitlab-com.gitlab.io/gl-infra/cloudflare-error-pages/cf_challenge.html"
  state      = "customized"
}

resource "cloudflare_custom_pages" "waf_challenge" {
  account_id = var.cloudflare_account_id
  type       = "waf_challenge"
  url        = "https://gitlab-com.gitlab.io/gl-infra/cloudflare-error-pages/cf_challenge.html"
  state      = "customized"
}

resource "cloudflare_custom_pages" "country_challenge" {
  account_id = var.cloudflare_account_id
  type       = "country_challenge"
  url        = "https://gitlab-com.gitlab.io/gl-infra/cloudflare-error-pages/cf_challenge.html"
  state      = "customized"
}

resource "cloudflare_custom_pages" "error_500" {
  account_id = var.cloudflare_account_id
  type       = "500_errors"
  url        = "https://gitlab-com.gitlab.io/gl-infra/cloudflare-error-pages/cf_500.html"
  state      = "customized"
}


resource "cloudflare_custom_pages" "error_1000" {
  account_id = var.cloudflare_account_id
  type       = "1000_errors"
  url        = "https://gitlab-com.gitlab.io/gl-infra/cloudflare-error-pages/cf_1000.html"
  state      = "customized"
}

resource "cloudflare_custom_pages" "always_online" {
  account_id = var.cloudflare_account_id
  type       = "always_online"
  url        = "https://gitlab-com.gitlab.io/gl-infra/cloudflare-error-pages/cf_always_online.html"
  state      = "customized"
}

resource "cloudflare_custom_pages" "under_attack" {
  account_id = var.cloudflare_account_id
  type       = "under_attack"
  url        = "https://gitlab-com.gitlab.io/gl-infra/cloudflare-error-pages/cf_under_attack.html"
  state      = "customized"
}

resource "cloudflare_custom_pages" "ip_block" {
  account_id = var.cloudflare_account_id
  type       = "ip_block"
  url        = "https://gitlab-com.gitlab.io/gl-infra/cloudflare-error-pages/cf_block.html"
  state      = "customized"
}

resource "cloudflare_custom_pages" "waf_block" {
  account_id = var.cloudflare_account_id
  type       = "waf_block"
  url        = "https://gitlab-com.gitlab.io/gl-infra/cloudflare-error-pages/cf_block.html"
  state      = "customized"
}

#######################
#
# codesandbox bucket
#
#######################

resource "google_storage_bucket" "codesandbox" {
  name          = "sandbox-prod.gitlab-static.net"
  location      = "US-EAST1"
  storage_class = "REGIONAL"
  website {
    main_page_suffix = "index.html"
    not_found_page   = "404.html"
  }
  labels = {
    tfmanaged = "yes"
    name      = "sandbox-prod-gitlab-static-net"
  }
}

resource "google_storage_bucket_iam_member" "codesandbox_objectViewer" {
  bucket = google_storage_bucket.codesandbox.name
  role   = "roles/storage.objectViewer"
  member = "allUsers"
}

module "codesandbox-dns-record" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = "gitlab-static.net."

  cname = {
    "sandbox-prod.gitlab-static.net." = {
      ttl     = "300"
      records = ["l3.shared.global.fastly.net"]
    }
  }
}

#######################
#
# Static objects cache
#
#######################

module "static-objects-cache" {
  cache_private_objects  = false
  cloudflare_zone_id     = var.cloudflare_net_zone_id
  elasticsearch_auth     = var.static_objects_cache_elasticsearch_auth
  elasticsearch_host     = "https://92c87c26b16049b0a30af16b94105528.us-central1.gcp.cloud.es.io:9243"
  enabled                = true
  entrypoint             = "static-objects.gitlab.net"
  environment            = var.environment
  external_storage_token = var.static_objects_external_storage_token
  mode                   = "aggressive"
  origin_host            = "gitlab.com"
  sentry_project_id      = "112"
  sentry_key             = var.static_objects_cache_sentry_key
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/static-objects-cache.git?ref=v2.11.1"
}

module "cf_allowlists" {
  # Intentionally not version pinned. See
  # https://ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/cf_allowlists#noteworthy-details
  source            = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/cf_allowlists.git"
  zone_id           = var.cloudflare_zone_id
  include_customers = true
}

########################
# Service account to allow a Patroni replica to take a GCS snapshot of its data disk
# Related to https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/11371
########################
resource "google_service_account" "database-snapshot" {
  account_id   = "database-snapshot"
  display_name = "Database snapshot"
  description  = "A service account to be used to trigger a snapshot for the data disk of a database replica"
}

resource "google_project_iam_custom_role" "database-snapshot" {
  project     = var.project
  description = "A role to allow a Patroni replica to take a snapshot of its data disk"
  role_id     = "PatroniReplicaSnapshot"
  title       = "Patroni Replica Snapshot"
  permissions = [
    "compute.disks.createSnapshot",
    "compute.disks.get",
    "compute.snapshots.create",
    "compute.snapshots.get",
    "compute.zoneOperations.get"
  ]
}

resource "google_project_iam_binding" "database-snapshot" {
  project = var.project
  role    = "projects/${var.project}/roles/${google_project_iam_custom_role.database-snapshot.role_id}"

  members = ["serviceAccount:${google_service_account.database-snapshot.email}"]
}

### Using for https://ops.gitlab.net/gitlab-com/gl-infra/gitlab-restore/gitaly-snapshot-restore
resource "google_compute_firewall" "iap_to_ssh" {
  name    = "ingress-allow-iap-to-ssh"
  network = var.environment

  direction = "INGRESS"
  priority  = 1000

  # Cloud IAP's TCP forwarding netblock
  source_ranges = ["35.235.240.0/20"]
  target_tags   = ["iap-tunnel"]

  allow {
    protocol = "tcp"
    ports    = [22]
  }
}
