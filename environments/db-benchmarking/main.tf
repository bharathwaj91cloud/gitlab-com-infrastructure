## State storage
terraform {
  backend "s3" {
    bucket = "gitlab-com-infrastructure"
    key    = "terraform/db-benchmarking/terraform.tfstate"
    region = "us-east-1"
  }
}

## AWS
provider "aws" {
  region  = "us-east-1"
  version = "~> 2.27.0"
}

## Local
provider "local" {
  version = "~> 1.4.0"
}

## Google

provider "google" {
  project = var.project
  region  = var.region
  version = "~> 3.44"
}

provider "google-beta" {
  project = var.project
  region  = var.region
  version = "~> 3.44"
}

## CloudFlare

provider "cloudflare" {
  version    = "= 2.5.1"
  email      = var.cloudflare_email
  api_key    = var.cloudflare_api_key
  account_id = var.cloudflare_account_id
}

provider "random" {
  version = "~> 2.2"
}

##################################
#
#  Network
#
#################################

module "network" {
  environment      = var.environment
  project          = var.project
  source           = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/vpc.git?ref=v2.0.0"
  internal_subnets = var.internal_subnets
}

##################################
#
#  NAT gateway
#
#################################

module "nat" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/cloud-nat.git?ref=v1.3.0"

  log_level    = "ALL"
  nat_ip_count = 1
  nat_name     = "nat-${var.environment}-${var.region}"
  network_name = module.network.name
  region       = var.region
  router_name  = "nat-router-${var.environment}-${var.region}"
}

##################################
#
#  Bastion
#
##################################

module "bastion" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-bastion]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  health_check          = "http"
  ip_cidr_range         = var.subnetworks["bastion"]
  machine_type          = var.machine_types["bastion"]
  name                  = "bastion"
  node_count            = var.node_count["bastion"]
  project               = var.project
  public_ports          = var.public_ports["bastion"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

#### Load balancer for bastion
module "gcp-tcp-lb-bastion" {
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs_bastion["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns_bastion
  gitlab_zone            = "gitlab.com."
  health_check_ports     = var.tcp_lbs_bastion["health_check_ports"]
  instances              = module.bastion.instances_self_link
  lb_count               = length(var.tcp_lbs_bastion["names"])
  name                   = "gcp-tcp-lb-bastion"
  names                  = var.tcp_lbs_bastion["names"]
  project                = var.project
  region                 = var.region
  session_affinity       = "CLIENT_IP"
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v4.1.1"
  targets                = ["bastion"]
}

##################################
#
#  Consul
#
##################################

module "consul" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-infra-consul]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  ip_cidr_range         = var.subnetworks["consul"]
  machine_type          = var.machine_types["consul"]
  name                  = "consul"
  node_count            = var.node_count["consul"]
  project               = var.project
  public_ports          = var.public_ports["consul"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 8300
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

#############################################
#
#  Patroni
#
#############################################

module "patroni" {
  assign_public_ip       = false
  bootstrap_data_disk    = false
  bootstrap_version      = 9
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-db-patroni]\""
  create_backend_service = true
  data_disk_size         = var.data_disk_sizes["patroni"]
  data_disk_type         = "pd-ssd"
  dns_zone_name          = var.dns_zone_name
  environment            = var.environment
  health_check           = "http"
  health_check_port      = "8009"
  log_disk_size          = "250"
  ip_cidr_range          = var.subnetworks["patroni"]
  machine_type           = var.machine_types["patroni"]
  name                   = "patroni"
  node_count             = var.node_count["patroni"]
  project                = var.project
  public_ports           = var.public_ports["patroni"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_path           = "/"
  service_port           = 6432
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor-with-group.git?ref=v4.1.1"
  tier                   = "db"
  use_new_node_name      = true
  vpc                    = module.network.self_link
  os_disk_size           = 100
}

module "patroni-zfs" {
  assign_public_ip       = false
  bootstrap_data_disk    = false
  bootstrap_version      = 9
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-db-patroni-zfs]\""
  create_backend_service = true
  data_disk_size         = var.data_disk_sizes["patroni"]
  data_disk_type         = "pd-ssd"
  dns_zone_name          = var.dns_zone_name
  environment            = var.environment
  health_check           = "http"
  health_check_port      = "8009"
  log_disk_size          = "250"
  ip_cidr_range          = var.subnetworks["patroni-zfs"]
  machine_type           = var.machine_types["patroni"]
  name                   = "patroni-zfs"
  node_count             = var.node_count["patroni-zfs"]
  project                = var.project
  public_ports           = var.public_ports["patroni"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_path           = "/"
  service_port           = 6432
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor-with-group.git?ref=v4.1.1"
  tier                   = "db"
  use_new_node_name      = true
  vpc                    = module.network.self_link
  os_disk_size           = 100
}

module "patroni-registry" {
  assign_public_ip       = false
  bootstrap_data_disk    = false
  bootstrap_version      = 9
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-db-patroni-registry]\""
  create_backend_service = true
  data_disk_size         = var.data_disk_sizes["patroni-registry"]
  data_disk_type         = "pd-ssd"
  dns_zone_name          = var.dns_zone_name
  environment            = var.environment
  health_check           = "http"
  health_check_port      = "8009"
  log_disk_size          = "250"
  ip_cidr_range          = var.subnetworks["patroni-registry"]
  machine_type           = var.machine_types["patroni-registry"]
  name                   = "patroni-v12-registry"
  node_count             = var.node_count["patroni-registry"]
  project                = var.project
  public_ports           = var.public_ports["patroni"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_path           = "/"
  service_port           = 6432
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor-with-group.git?ref=v4.1.1"
  tier                   = "db"
  use_new_node_name      = true
  vpc                    = module.network.self_link
  os_disk_size           = 100
}

module "patroni-zfs-registry" {
  assign_public_ip       = false
  bootstrap_data_disk    = false
  bootstrap_version      = 9
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-db-patroni-zfs-registry]\""
  create_backend_service = true
  data_disk_size         = var.data_disk_sizes["patroni-registry"]
  data_disk_type         = "pd-ssd"
  dns_zone_name          = var.dns_zone_name
  environment            = var.environment
  health_check           = "http"
  health_check_port      = "8009"
  log_disk_size          = "250"
  ip_cidr_range          = var.subnetworks["patroni-zfs-registry"]
  machine_type           = var.machine_types["patroni-registry"]
  name                   = "patroni-v12-zfs-registry"
  node_count             = var.node_count["patroni-zfs-registry"]
  project                = var.project
  public_ports           = var.public_ports["patroni"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_path           = "/"
  service_port           = 6432
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor-with-group.git?ref=v4.1.1"
  tier                   = "db"
  use_new_node_name      = true
  vpc                    = module.network.self_link
  os_disk_size           = 100
}

#############################################
#
#  PgBouncer
#
#############################################

module "pgbouncer" {
  assign_public_ip       = false
  bootstrap_version      = var.bootstrap_script_version
  chef_init_run_list     = "\"recipe[gitlab-server::hack_gitlab_ctl_reconfigure]\""
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-db-pgbouncer]\""
  create_backend_service = true
  dns_zone_name          = var.dns_zone_name
  egress_ports           = var.egress_ports
  environment            = var.environment
  health_check           = "tcp"
  ip_cidr_range          = var.subnetworks["pgbouncer"]
  machine_type           = var.machine_types["pgbouncer"]
  name                   = "pgbouncer"
  node_count             = var.node_count["pgbouncer"]
  project                = var.project
  public_ports           = var.public_ports["pgbouncer"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_port           = 6432
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                   = "db"
  use_new_node_name      = true
  vpc                    = module.network.self_link
}

module "pgbouncer-sidekiq" {
  assign_public_ip       = false
  backend_service_type   = "regional"
  bootstrap_version      = var.bootstrap_script_version
  chef_init_run_list     = "\"recipe[gitlab-server::hack_gitlab_ctl_reconfigure]\""
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-db-pgbouncer-sidekiq]\""
  create_backend_service = true
  dns_zone_name          = var.dns_zone_name
  environment            = var.environment
  health_check           = "tcp"
  ip_cidr_range          = var.subnetworks["pgbouncer-sidekiq"]
  machine_type           = var.machine_types["pgbouncer"]
  name                   = "pgbouncer-sidekiq"
  node_count             = var.node_count["pgbouncer-sidekiq"]
  project                = var.project
  public_ports           = var.public_ports["pgbouncer"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_port           = 6432
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                   = "db"
  use_new_node_name      = true
  vpc                    = module.network.self_link
}

module "pgbouncer-registry" {
  assign_public_ip       = false
  bootstrap_version      = var.bootstrap_script_version
  chef_init_run_list     = "\"recipe[gitlab-server::hack_gitlab_ctl_reconfigure]\""
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-db-pgbouncer-registry]\""
  create_backend_service = true
  dns_zone_name          = var.dns_zone_name
  egress_ports           = var.egress_ports
  environment            = var.environment
  health_check           = "tcp"
  ip_cidr_range          = var.subnetworks["pgbouncer-registry"]
  machine_type           = var.machine_types["pgbouncer-registry"]
  name                   = "pgbouncer-registry"
  node_count             = var.node_count["pgbouncer-registry"]
  project                = var.project
  public_ports           = var.public_ports["pgbouncer"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_port           = 6432
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                   = "db"
  use_new_node_name      = true
  vpc                    = module.network.self_link
}

#############################################
#
#  JMeter
#
#############################################

module "jmeter" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_init_run_list    = "\"recipe[gitlab-server::hack_gitlab_ctl_reconfigure]\""
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-infra-jmeter]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  ip_cidr_range         = var.subnetworks["jmeter"]
  machine_type          = var.machine_types["jmeter"]
  name                  = "jmeter"
  node_count            = var.node_count["jmeter"]
  project               = var.project
  public_ports          = var.public_ports["jmeter"]
  region                = var.region
  service_account_email = var.service_account_email
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Service Accounts
#
##################################

# Create the bootstrap KMS key ring
resource "google_kms_key_ring" "bootstrap" {
  name     = "gitlab-${var.environment}-bootstrap"
  location = "global"
  project  = var.project
}

resource "google_kms_key_ring_iam_binding" "bootstrap" {
  key_ring_id = "${var.project}/global/gitlab-${var.environment}-bootstrap"
  role        = "roles/cloudkms.cryptoKeyDecrypter"

  members = [
    "serviceAccount:${var.service_account_email}",
  ]
}

resource "google_kms_crypto_key" "bootstrap-validation" {
  name            = "gitlab-${var.environment}-bootstrap-validation"
  key_ring        = google_kms_key_ring.bootstrap.id
  rotation_period = "7776000s" # 90 days

  lifecycle {
    prevent_destroy = true
  }
}

resource "google_kms_key_ring" "secrets" {
  name     = "gitlab-secrets"
  location = "global"
  project  = var.project
}

resource "google_kms_key_ring_iam_binding" "secrets" {
  key_ring_id = "${var.project}/global/gitlab-secrets"
  role        = "roles/cloudkms.cryptoKeyDecrypter"

  members = [
    "serviceAccount:${var.service_account_email}",
  ]
}

resource "google_kms_crypto_key" "secrets" {
  name            = var.environment
  key_ring        = google_kms_key_ring.secrets.id
  rotation_period = "7776000s" # 90 days

  lifecycle {
    prevent_destroy = true
  }
}

##################################
#
#  Object storage buckets
#
##################################

module "gitlab_object_storage" {
  environment                       = var.environment
  service_account_email             = var.service_account_email
  gcs_service_account_email         = var.gcs_service_account_email
  gcs_storage_analytics_group_email = var.gcs_storage_analytics_group_email
  source                            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/storage-buckets.git?ref=v5.13.0"
  project                           = var.project
}

##################################
#
#  Monitoring
#
#  Uses the monitoring module, this
#  creates a single instance behind
#  a load balancer with identity aware
#  proxy enabled.
#
##################################

resource "google_compute_subnetwork" "monitoring" {
  ip_cidr_range            = var.subnetworks["monitoring"]
  name                     = format("monitoring-%v", var.environment)
  network                  = module.network.self_link
  private_ip_google_access = true
  project                  = var.project
  region                   = var.region
}

#######################
#
# load balancer for all hosts in this section
#
#######################

module "monitoring-lb" {
  environment     = var.environment
  dns_zone_id     = var.gitlab_net_zone_id
  dns_zone_name   = "gitlab.net"
  dns_zone_prefix = "db-benchmarking."
  hosts           = var.monitoring_hosts["names"]
  name            = "monitoring-lb"
  project         = var.project
  region          = var.region
  service_ports   = var.monitoring_hosts["ports"]
  source          = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/https-lb.git?ref=v3.0.1"
  subnetwork_name = google_compute_subnetwork.monitoring.name
  targets         = var.monitoring_hosts["names"]
  url_map         = google_compute_url_map.monitoring-lb.self_link
}

#######################
module "prometheus" {
  bootstrap_version = var.bootstrap_script_version
  chef_provision    = var.chef_provision
  chef_run_list     = "\"role[${var.environment}-infra-prometheus]\""
  data_disk_size    = var.data_disk_sizes["prometheus"]
  data_disk_type    = "pd-standard"
  dns_zone_name     = var.dns_zone_name
  environment       = var.environment
  fw_whitelist_subnets = concat(
    var.monitoring_whitelist_prometheus["subnets"],
    var.other_monitoring_subnets,
  )
  fw_whitelist_ports    = var.monitoring_whitelist_prometheus["ports"]
  machine_type          = var.machine_types["monitoring"]
  name                  = "prometheus"
  node_count            = var.node_count["prometheus"]
  oauth2_client_id      = var.oauth2_client_id_monitoring
  oauth2_client_secret  = var.oauth2_client_secret_monitoring
  persistent_disk_path  = "/opt/prometheus"
  project               = var.project
  public_ports          = ["22"]
  region                = var.region
  service_account_email = var.service_account_email
  service_ports = [
    {
      "name" : "prometheus",
      "port" : element(
        var.monitoring_hosts["ports"],
        index(var.monitoring_hosts["names"], "prometheus"),
      ),
      "health_check_path" : "/graph",
    },
  ]
  source            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v5.0.0"
  subnetwork_name   = google_compute_subnetwork.monitoring.name
  tier              = "inf"
  use_external_ip   = true
  use_new_node_name = true
  vpc               = module.network.self_link
}

module "sd-exporter" {
  assign_public_ip          = false
  additional_scopes         = ["https://www.googleapis.com/auth/monitoring"]
  allow_stopping_for_update = false
  bootstrap_version         = var.bootstrap_script_version
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-infra-sd-exporter]\""
  create_backend_service    = false
  dns_zone_name             = var.dns_zone_name
  environment               = var.environment
  machine_type              = var.machine_types["sd-exporter"]
  name                      = "sd-exporter"
  node_count                = var.node_count["sd-exporter"]
  project                   = var.project
  public_ports              = var.public_ports["sd-exporter"]
  region                    = var.region
  service_account_email     = var.service_account_email
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  subnetwork_name           = google_compute_subnetwork.monitoring.name
  tier                      = "inf"
  use_new_node_name         = true
  vpc                       = module.network.self_link
}

## Allow the organization to go through the IAPs
resource "google_project_iam_member" "iap_domain" {
  project = var.project
  role    = "roles/iap.httpsResourceAccessor"
  member  = "domain:${var.domain}"
}

##################################
#
#  PubSub
#
##################################

module "pubsub" {
  source                = "../../modules/pubsub"
  environment           = var.environment
  project               = var.project
  pubsub_topics         = var.pubsub_topics
  pubsub_filters        = var.pubsub_filters
  service_account_email = var.service_account_email
}

##################################
#
#  Pubsubbeat for GKE
#
##################################

# Give the GCP service account relevant PubSub permissions (assign roles)
resource "google_project_iam_member" "pubsubbeat-pubsubbeat-pubsub-subscriber" {
  project = var.project
  role    = "roles/pubsub.subscriber"
  member  = "serviceAccount:${var.pubsubbeat-k8s-sa["email"]}"
}

resource "google_project_iam_member" "pubsubbeat-pubsubbeat-pubsub-editor" {
  project = var.project
  role    = "roles/pubsub.editor"
  member  = "serviceAccount:${var.pubsubbeat-k8s-sa["email"]}"
}

# Service account utilized by CI for Read Only operations
resource "google_project_iam_custom_role" "k8s-workloads" {
  project     = var.project
  role_id     = "k8sWorkloads"
  title       = "k8s-workloads"
  permissions = ["clientauthconfig.clients.listWithSecrets", "container.secrets.list"]
}

resource "google_project_iam_binding" "k8s-workloads" {
  project = var.project
  role    = "projects/${var.project}/roles/${google_project_iam_custom_role.k8s-workloads.role_id}"

  members = [
    "serviceAccount:${var.k8s-workloads-ro-sa["email"]}",
  ]
}

resource "google_project_iam_member" "pubsub_editor" {
  project = var.project
  role    = "roles/pubsub.editor"
  member  = "serviceAccount:${var.service_account_email}"
}

resource "google_project_iam_member" "logging_logWriter" {
  project = var.project
  role    = "roles/logging.logWriter"
  member  = "serviceAccount:${var.service_account_email}"
}

##################################
#
#  Console
#
##################################

module "console" {
  assign_public_ip      = true
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-console-node]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.console_egress_ports
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["console"]
  machine_type          = var.machine_types["console"]
  name                  = "console"
  node_count            = var.node_count["console"]
  os_disk_size          = 50
  project               = var.project
  public_ports          = var.public_ports["console"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.1.1"
  tier                  = "sv"
  use_external_ip       = false
  use_new_node_name     = true
  vpc                   = module.network.self_link
}
