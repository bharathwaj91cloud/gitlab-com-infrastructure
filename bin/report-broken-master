#!/usr/bin/env ruby

require 'net/http'
require 'uri'
require 'json'

raise 'missing env vars' unless ENV['CI_ENVIRONMENT_NAME'] && ENV['CI_JOB_URL'] && ENV['CI_PIPELINE_URL'] && ENV['SLACK_WEBHOOK_URL']

slack_json = {
  "blocks": [
    {
      "type": "section",
      "text": {
        "type": "mrkdwn",
        "text": ":terraform::warning: Terraform plan for *#{ENV['CI_ENVIRONMENT_NAME']}* is out-of-sync with the master branch."
      }
    },
    {
      "type": "actions",
      "elements": [
        {
          "type": "button",
          "text": {
            "type": "plain_text",
            "text": ":hammer: Pipeline"
          },
          "style": "primary",
          "url": ENV['CI_PIPELINE_URL']
        },
        {
          "type": "button",
          "text": {
            "type": "plain_text",
            "text": ":scroll: Plan diff"
          },
          "url": ENV['CI_JOB_URL']
        },
        {
          "type": "button",
          "text": {
            "type": "plain_text",
            "text": ":scroll: Runbook"
          },
          "url": "https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/uncategorized/terraform-broken-master.md"
        },
      ]
    }
  ]
}

res = Net::HTTP.post(
  URI(ENV['SLACK_WEBHOOK_URL']),
  slack_json.to_json,
  'Content-Type' => 'application/json',
)
puts res.body

exit 1
